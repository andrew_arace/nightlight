#include <Adafruit_NeoPixel.h>

#define LIGHT_PIN 1
#define BUTTON 0
#define HOLD_THRESHOLD 20 //long press detection
#define COLOR_CYCLE_STEP 1 //rainbow sweep increments

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(2, LIGHT_PIN, NEO_GRB + NEO_KHZ800);
uint8_t buttonState = HIGH; //pullup resistor default state
uint8_t lastButtonState = HIGH; //temp last state of button
uint8_t buttonHeld = 0; //long press temp variable
uint8_t tapCount = 0; //tap counter for brightness mode
bool longPress = false; //indicates if currently long pressing
uint32_t setColor; //color that will be applied at end of loop()
uint8_t color = 85; //starting color for cycling (red)
uint32_t t = 0;
uint32_t shutoffPeriod = 1000*60*60; //1 hour

void setup() {
  pinMode(BUTTON, INPUT_PULLUP); // switch wired to ground & pin, pushing makes it go LOW
  pixels.setBrightness(255);
  pixels.begin();
  pixels.show(); // Initialize all pixels to 'off'
  setColor = pixels.Color(0,0,0);
  t = millis();
}

void loop() {
  checkButton();
  checkTimer();
  colorWipe(setColor, 0);
}

void checkTimer() {
  if(t == 0) return;
  if(millis() - t > shutoffPeriod) {
    t = millis();
    setColor = pixels.Color(0,0,0);
    tapCount = 0;
  }
}

bool checkButton() {
  buttonState = digitalRead(BUTTON);
  delay(10);
  
  if(buttonState != lastButtonState) {
    t = millis();
    lastButtonState = buttonState;
    buttonHeld = 0;
    longPress = false;
    
    if(buttonState == LOW) {
      //just pushed
      //check for long button press/hold
      while (digitalRead(BUTTON) == LOW && buttonHeld < (HOLD_THRESHOLD+1)){
        delay(50);
        buttonHeld++;
      }
  
      if (buttonHeld > HOLD_THRESHOLD) {
        //long press
        tapCount = 0;
        longPress = true;
      }
      else {
        tapCount++;
        //was not a long enough hold
        if(tapCount > 3) {
          setColor = pixels.Color(0,0,0);
          tapCount = 0;
        }
        else if(tapCount > 1) {
          setColor = pixels.Color(255-(tapCount*64-1),255-(tapCount*64-1),255-(tapCount*64-1));
        }
        else {
          setColor = pixels.Color(255,255,255);
        }
      }
    }
    else {
      longPress = false;
    }
  }
  
  if(longPress) {
    //still holding the button
    color = (color >= 255 ? 0 : color += COLOR_CYCLE_STEP);
    delay(100);
    setColor = Wheel(color);
  }
  
  buttonHeld = 0;
  return false;
}

void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<pixels.numPixels(); i++) {
      pixels.setPixelColor(i, c);
      pixels.show();
      delay(wait);
  }
}

uint32_t Wheel(byte WheelPos) {
  if(WheelPos < 85) {
   return pixels.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if(WheelPos < 170) {
   WheelPos -= 85;
   return pixels.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170;
   return pixels.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}
