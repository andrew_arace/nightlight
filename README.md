# enzo's nightlight

Materials:

- Adafruit GEMMA 
- two Adafruit NeoPixels
- momentary button
- 10' USB cord & usb wall wart

## Operation

- Initial mode, off
- Momentary Button Press: change to full white
- Momentary Button Press: Change to 75% brightness
- Momentary Button Press: Change to 50% brightness
- Momentary Button Press: turn off
- Long Press Button (and hold): full brightness, and enter color cycle mode
    - Lights will slowly transition through rainbow of colors
    - Letting go of the button will stop at the selected color
    - Momentary press button and release to switch back to full white mode
- Any ON mode will shut off automatically after 1 hour

Andrew Arace

2018-04-11
